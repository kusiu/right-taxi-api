﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RightTaxi.API.Models;
using RightTaxi.API.Services;

namespace RightTaxi.API.Controllers
{
    [ApiController]
    [Route("api/coordinates")]
    public class CoordinatesController : ControllerBase
    {
        private GeoCodingService _geoCodingService;

        public CoordinatesController(GeoCodingService GeoCodingService)
        {
            _geoCodingService = GeoCodingService;
        }

        [HttpGet("{location}")]
        public async Task<Address> Get(string location)
        {
            var address = await _geoCodingService.GetCoordinatesTask(location);    
            return address;
        }
    }
}
