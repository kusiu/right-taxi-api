namespace RightTaxi.API.Models
{
    public class Address
    {
        public string Loc {get; set;}
        public double Lat {get; set;}
        public double Lon {get; set;}
    }
}