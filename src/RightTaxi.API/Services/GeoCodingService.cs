using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using RightTaxi.API.Models;

namespace RightTaxi.API.Services
{
    public class GeoCodingService
    {
        private const string ENDPOINT_URL = "https://eu1.locationiq.com/v1/search.php?key={0}&q={1}&format=json";

        private const string API_KEY = "2ae142e062afe6";

        public async Task<Address> GetCoordinatesTask(string Location)
        {
            string url = string.Format(ENDPOINT_URL, API_KEY, Location);

            using(var client = HttpClientFactory.Create())
            {
                using (HttpResponseMessage response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        List<Address> addressList = await response.Content.ReadAsAsync<List<Address>>();
                        Address address = addressList[0];
                        address.Loc = Location;
                        return address;
                    }
                    else 
                    {
                        throw new Exception(response.ReasonPhrase);
                    }
                }
            }                 
        }
    }
}