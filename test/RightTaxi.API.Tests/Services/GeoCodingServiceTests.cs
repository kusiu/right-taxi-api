using RightTaxi.API.Models;
using RightTaxi.API.Services;
using Xunit;
using System.Net.Http;

namespace RightTaxi.API.Tests.Services
{
    public class GeoCodingServiceTests
    {
        private GeoCodingService _geoCodingService;

        public GeoCodingServiceTests()
        {
            _geoCodingService = new GeoCodingService();
        }

        [Fact]
        public async void GetCoordinates_check_coordinates_of_SE10_0GP()
        {
            // Arrange
            Address expected = new Address();
            expected.Lat = 51.4964205832184;
            expected.Lon = 0.0137767327947096;
            
            // Act
            Address actual = await _geoCodingService.GetCoordinatesTask("SE10 0GP");
            
            // Assert
            Assert.Equal(expected.Lat, actual.Lat);
            Assert.Equal(expected.Lon, actual.Lon);
        }
    }
}